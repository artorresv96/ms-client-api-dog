package com.ms.api.dog.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ApiDogEntity {

    DetailBreed detailBreed;

    Breed breed;
    
    @BeforeEach
    public void setUp() {
        detailBreed = new DetailBreed();
        detailBreed.setBreed("bulldog");
        detailBreed.setImages(new ArrayList<>());
        
        ImageBreed imageBreed = new ImageBreed();
        imageBreed.setUrl("https://images.dog.ceo/breeds/bulldog-boston/20200710_175933.jpg");
        
        detailBreed.getImages().add(imageBreed);
        detailBreed.setSubBreeds(new ArrayList<>());
        detailBreed.getSubBreeds().add("english");
        
        
        breed = new Breed();
        breed.setCode(200);
        breed.setMessage(new HashMap<>());
        List<String> message = new ArrayList<>();
        message.add("english");
        breed.getMessage().put("bulldog", message);
        breed.setStatus("success");
        
        
    }
    
    @Test
    public void newDetailBreedTest() {
        
       assertEquals(1, detailBreed.getSubBreeds().size());
       assertEquals(1, detailBreed.getImages().size());
       assertEquals("bulldog", detailBreed.getBreed());
       
    }
    
    @Test
    public void newBreedTest() {
        assertEquals(1, breed.getMessage().size());
        assertEquals(200, breed.getCode());
        assertEquals("success", breed.getStatus());
        
    }
    
}
