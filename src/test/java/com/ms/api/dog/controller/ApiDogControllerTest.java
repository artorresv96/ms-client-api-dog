package com.ms.api.dog.controller;
import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.ms.api.dog.entity.DetailBreed;
import com.ms.api.dog.service.IApiDogService;

@SpringBootTest
public class ApiDogControllerTest {

    @InjectMocks
    ApiDogController apiDogController;
    
    @Mock
    IApiDogService apiDogService;
    
    DetailBreed detailBreed;

    @Test
    public void obtainBreedTest() throws Exception {
        apiDogController = new ApiDogController("https://localhost/all", "https://localhost/one");
        apiDogController.apiDogService = Mockito.spy(IApiDogService.class);
        
        Mockito.when(apiDogController.apiDogService.obtainBreed(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(new DetailBreed());
        ResponseEntity<DetailBreed> rs = apiDogController.obtainBreed("bulldog");
        assertEquals("status <<200>>", HttpStatus.OK, rs.getStatusCode());
    }
    
//    @Test
//    public void obtainBreedExceptionTest() throws Exception {
//        apiDogController = new ApiDogController(null, null);
//        apiDogController.apiDogService = Mockito.spy(IApiDogService.class);
//        Exception exception = null;    
//        
//        try{
//            Mockito.when(apiDogController.obtainBreed(Mockito.anyString())).thenThrow(new Exception("Error de comunicacion con servidor de congfiguaraciones"));
//        }catch(Exception e) {
//            exception = e;
//        }
//        
//        
//        assertEquals("Error de comunicacion con servidor de congfiguaraciones", exception.getMessage());
//    }
    

}
