package com.ms.api.dog.impl;

//import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ms.api.dog.entity.Breed;
import com.ms.api.dog.entity.ImagesBreed;

import lombok.extern.slf4j.Slf4j;

@SpringBootTest
@Slf4j
public class ApiDogImplTest {

    @InjectMocks
    @Spy
    ApiDogImpl apiDogImpl;

    @Mock
    private RestTemplate restTemplate;

    Breed breed;

    Breed breedNull;

    ImagesBreed imagesBreed;

    ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    public void before() throws JsonParseException, JsonMappingException, FileNotFoundException, IOException {

        breed = mapper.readValue(new FileInputStream("src/test/resources/RS-ALL-BREEDS.json"),
                new TypeReference<Breed>() {
                });
        breedNull = mapper.readValue(new FileInputStream("src/test/resources/RS-BREEDS-NOT-FOUND.json"),
                new TypeReference<Breed>() {
                });
        imagesBreed = mapper.readValue(new FileInputStream("src/test/resources/RS-IMAGES-BREED.json"),
                new TypeReference<ImagesBreed>() {
                });

    }

    @Test
    public void obtainBreedTest() {

        Mockito.when(restTemplate.exchange("", HttpMethod.GET, null, new ParameterizedTypeReference<Breed>() {
        })).thenReturn(new ResponseEntity<Breed>(breed, HttpStatus.ACCEPTED));

        Mockito.when(restTemplate.exchange("", HttpMethod.GET, null, new ParameterizedTypeReference<ImagesBreed>() {
        })).thenReturn(new ResponseEntity<ImagesBreed>(imagesBreed, HttpStatus.ACCEPTED));

        assertNotNull("Respuesta valida ",
                apiDogImpl.obtainBreed(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()));
    }

}
