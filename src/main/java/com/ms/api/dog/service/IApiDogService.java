package com.ms.api.dog.service;

import com.ms.api.dog.entity.Breed;
import com.ms.api.dog.entity.DetailBreed;
import com.ms.api.dog.entity.ImagesBreed;

public interface IApiDogService {

    DetailBreed obtainBreed(String breed, String urlAll, String urlOne);
    Breed obtainAllBreed(String urlAllBreed);
    ImagesBreed obtainImages(String breed, String urlImages);
    
}
