package com.ms.api.dog.entity;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImagesBreed implements Serializable{

    private static final long serialVersionUID = 7883463072722331764L;
    
    @Getter @Setter private List<ImageBreed> message;
    @Getter @Setter private String status;
    @Getter @Setter private int code;
}
