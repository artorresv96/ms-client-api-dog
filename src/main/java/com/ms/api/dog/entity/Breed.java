package com.ms.api.dog.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Breed implements Serializable{


    private static final long serialVersionUID = 1999375030300813693L;
    
    @Getter @Setter private String status;
    @Getter @Setter private HashMap<String,List<String>> message;
    @Getter @Setter private int code;
}
