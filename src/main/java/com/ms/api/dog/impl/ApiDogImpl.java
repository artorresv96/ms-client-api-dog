package com.ms.api.dog.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.ms.api.dog.entity.Breed;
import com.ms.api.dog.entity.DetailBreed;
import com.ms.api.dog.entity.ImagesBreed;
import com.ms.api.dog.service.IApiDogService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ApiDogImpl implements IApiDogService {

    @Autowired
    RestTemplate restTemplate;

    private static final String BREED_STATUS = "Breed not found";
    private static final int CODE_NOT_FOUND = 404;
    private static final String BREED_NAME = "{breed name}";

    @Override
    public DetailBreed obtainBreed(String breed, String urlAll, String urlOne) {

        DetailBreed detailBreed = new DetailBreed();

        detailBreed.setBreed(breed);
        detailBreed.setSubBreeds(Optional.of(this.obtainAllBreed(urlAll).getMessage()).get().get(breed));
        detailBreed.setImages(this.obtainImages(breed, urlOne).getMessage());

        return detailBreed;
    }

    @Override
    public Breed obtainAllBreed(String urlAllBreed) {

        Breed breed = new Breed();

        breed.setCode(CODE_NOT_FOUND);
        breed.setMessage(new HashMap<String, List<String>>());
        breed.setStatus(BREED_STATUS);

        try {

            HttpEntity<Breed> response = restTemplate.exchange(urlAllBreed, HttpMethod.GET, null,
                    new ParameterizedTypeReference<Breed>() {
                    });

            breed = Optional.ofNullable(response.getBody()).orElse(breed);

        } catch (HttpClientErrorException e) {
            log.error("Error obtainAllBreed -> " + e.getMessage());
        }

        return breed;
    }

    @Override
    public ImagesBreed obtainImages(String breed, String urlImages) {

        ImagesBreed imagesBreed = new ImagesBreed();
        imagesBreed.setMessage(new ArrayList<>());
        imagesBreed.setStatus(BREED_STATUS);
        imagesBreed.setCode(CODE_NOT_FOUND);

        try {

            HttpEntity<ImagesBreed> response = restTemplate.exchange(urlImages.replace(BREED_NAME, breed),
                    HttpMethod.GET, null, new ParameterizedTypeReference<ImagesBreed>() {
                    });

            imagesBreed = Optional.ofNullable(response.getBody()).orElse(imagesBreed);

        } catch (HttpClientErrorException e) {

            log.error("Error obtainImages -> " + e.getResponseBodyAsString());
        }

        return imagesBreed;
    }

}
