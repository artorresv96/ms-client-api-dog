package com.ms.api.dog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ms.api.dog.entity.DetailBreed;
import com.ms.api.dog.service.IApiDogService;

import lombok.extern.slf4j.Slf4j;

@RefreshScope
@RestController
@RequestMapping("/api/dog/v1")
@Slf4j
public class ApiDogController {

    @Autowired
    IApiDogService apiDogService;
    
    private String urlAllBreed;
    
    private String urlImages;
    
    public ApiDogController(@Value("${url.api.list.all.breed}") String urlAllBreed, 
            @Value("${url.api.list.images}") String urlImages) {
        this.urlAllBreed = urlAllBreed;
        this.urlImages = urlImages;
    }
    @GetMapping("/obtain-breed")
    public ResponseEntity<DetailBreed> obtainBreed(@RequestParam(name = "breed", required = true) String breed) throws Exception {
        
        if(null != this.urlAllBreed && null != this.urlImages ) {
            return new ResponseEntity<>(apiDogService.obtainBreed(breed, urlAllBreed, urlImages), HttpStatus.OK);
        }else {
            throw new Exception("Error de comunicacion con servidor de congfiguaraciones");
        }
      
    }
}
