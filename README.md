# Microservicio cliente

_MS obtiene las url expuestas por el ms de configuracion y apartir de alli iniciar por el puerto
obtenido y logra comunicarse con otras api_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Pre-requisitos 📋

_necesitas tener instalado **JAVA 8**   y  **Maven  3.6**_

```
java  =  https://openjdk.java.net/install/
maven =  https://maven.apache.org/ref/3.6.3/
```

### Instalación 🔧

```
mvn clean package
```

ejecutar

```
java  -jar  target/*.jar
```

Probar la API

iniciar el explorador de su equipo Chrome o Firefox  y con la url siguiente.

```
http://localhost:8099/api/dog/v1/obtain-breed?breed=bulldog
```

